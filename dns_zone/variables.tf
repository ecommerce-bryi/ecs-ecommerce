variable "domain_name" {
  default = "bryidomain.tk"
}

variable "alias_domain_name" {
  default = "alb.bryidomain.tk"
}

variable "alb_domain_name" {
  type = string
}

variable "alb_zone_id" {
  type = string
}