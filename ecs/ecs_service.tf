resource "null_resource" "migrations" {
  provisioner "local-exec" {
    command = "aws ecs run-task --cluster ${var.app_name} --task-definition ${var.app_name}-migrations --count 1 --launch-type FARGATE --network-configuration 'awsvpcConfiguration={subnets=[${element(var.private_subnet_ids, 0)}],securityGroups=[${join(",", var.aws_security_group_ecs_tasks_id)}]}'"
  }
  depends_on = [
    aws_ecs_cluster.main,
    aws_ecs_task_definition.migrations,
    aws_iam_role.task_role,
    aws_iam_role.main_ecs_tasks,
    aws_iam_role_policy.main_ecs_tasks
  ]
}

resource "null_resource" "collectstatic" {
  provisioner "local-exec" {
    command = "aws ecs run-task --cluster ${var.app_name} --task-definition ${var.app_name}-collectstatic --count 1 --launch-type FARGATE --network-configuration 'awsvpcConfiguration={subnets=[${element(var.private_subnet_ids, 0)}],securityGroups=[${join(",", var.aws_security_group_ecs_tasks_id)}]}'"
  }
  depends_on = [
    aws_ecs_cluster.main,
    aws_ecs_task_definition.collectstatic,
    aws_iam_role.task_role,
    aws_iam_role.main_ecs_tasks,
    aws_iam_role_policy.main_ecs_tasks
  ]
}

resource "aws_ecs_service" "api" {
  name            = "${var.app_name}-api"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.api.family
  force_new_deployment = true
  desired_count   = var.api_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = var.aws_security_group_ecs_tasks_id
    subnets         = var.private_subnet_ids
  }

  load_balancer {
    target_group_arn = var.nlb_target_group_arn_api
    container_name   = "${var.app_name}-api"
    container_port   = var.app_port
  }

  depends_on = [
    aws_ecs_cluster.main,
    aws_ecs_task_definition.api,
    null_resource.migrations
  ]
}

resource "aws_ecs_service" "frontend" {
  name            = "${var.app_name}-frontend"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.frontend.family
  desired_count   = var.frontend_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = var.aws_security_group_ecs_tasks_id
    subnets         = var.private_subnet_ids
  }

  load_balancer {
    target_group_arn = var.alb_target_group_arn_frontend
    container_name   = "${var.app_name}-frontend"
    container_port   = 80
  }

  depends_on = [
    aws_ecs_cluster.main,
    aws_ecs_task_definition.frontend,
    aws_ecs_service.api,
    null_resource.migrations
  ]
}
