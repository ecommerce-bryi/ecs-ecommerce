resource "aws_ecs_cluster" "main" {
  name = var.name

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
  
  tags = {
    Name = var.cluster_tag_name
  }
}