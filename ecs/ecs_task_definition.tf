resource "aws_ecs_task_definition" "api" {
  family                   = "${var.app_name}-api"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}-api",
      image : var.api_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      entrypoint: ["./entrypoint.sh"],
      #command : ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
      portMappings : [
        {
          containerPort : var.app_port
          protocol : "tcp",
          hostPort : var.app_port
        }
      ],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-api",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "f_)*$$6xz#a7k(6ir&u@+tq8h@_t_9%3nr%9g5z4vdp#*a4)a*o"
          },
          {
            name: "STRIPE_SECRET_KEY",
            value: "sk_test_51HIHiuKBJV2qeWbD4IBpAODack7r7r9LJ0Y65zSFx7jUUwgy2nfKEgQGvorv1p2xp7tgMsJ5N9EW7K1lBdPnFnyK00kdrS27cj"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "S3ACCESSKEYID",
            value: var.s3accesskey
          },
          {
            name: "S3ACCESSKEY",
            value: var.s3secretkey
          },
          {
            name: "S3BUCKETNAME",
            value: var.s3bucketname
          },
          {
            name: "S3REGION",
            value: var.s3region
          },
      ],
    }
  ])
}

resource "aws_ecs_task_definition" "migrations" {
  family                   = "${var.app_name}-migrations"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}-migrations",
      image : var.api_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      entrypoint: ["./migrations.sh"],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-migrations",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "f_)*$$6xz#a7k(6ir&u@+tq8h@_t_9%3nr%9g5z4vdp#*a4)a*o"
          },
          {
            name: "STRIPE_SECRET_KEY",
            value: "sk_test_51HIHiuKBJV2qeWbD4IBpAODack7r7r9LJ0Y65zSFx7jUUwgy2nfKEgQGvorv1p2xp7tgMsJ5N9EW7K1lBdPnFnyK00kdrS27cj"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "S3ACCESSKEYID",
            value: var.s3accesskey
          },
          {
            name: "S3ACCESSKEY",
            value: var.s3secretkey
          },
          {
            name: "S3BUCKETNAME",
            value: var.s3bucketname
          },
          {
            name: "S3REGION",
            value: var.s3region
          },
      ],
    }
  ])
}

resource "aws_ecs_task_definition" "collectstatic" {
  family                   = "${var.app_name}-collectstatic"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}-collectstatic",
      image : var.api_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      #entrypoint: ["./migrations.sh"],
      command : ["python3", "manage.py", "collectstatic", "--noinput"],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-collectstatic",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "f_)*$$6xz#a7k(6ir&u@+tq8h@_t_9%3nr%9g5z4vdp#*a4)a*o"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "MEDIA_SERVER",
            value: "https://bryi-django-static.s3-website.eu-north-1.amazonaws.com"
          },
          {
            name: "S3ACCESSKEYID",
            value: var.s3accesskey
          },
          {
            name: "S3ACCESSKEY",
            value: var.s3secretkey
          },
          {
            name: "S3BUCKETNAME",
            value: var.s3bucketname
          },
          {
            name: "S3REGION",
            value: var.s3region
          },
      ],
    }
  ])
}

resource "aws_ecs_task_definition" "frontend" {
  family                   = "${var.app_name}-frontend"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}-frontend",
      image : var.frontend_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      portMappings : [
        {
          containerPort : 80
          protocol : "tcp",
          hostPort : 80
        }
      ],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-frontend",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
    }
  ])
}