terraform {
  backend "s3" {
    bucket = "ecs-bryi"
    key    = "store/terraform.tfstate"
    region = "us-east-1"
  }
}

data "aws_availability_zones" "available" {
}

module "vpc-prod" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                         = var.app_name
  cidr                         = var.vpc_cidr_block
  azs                          = data.aws_availability_zones.available.names
  create_database_subnet_group = true
  private_subnets              = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets               = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  database_subnets             = ["10.0.7.0/24", "10.0.8.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    App = "${var.app_name}"
  }
}

resource "random_string" "rds_password" {
  length           = 12
  special          = true
  override_special = "!#$&"

  keepers = {
    kepeer1 = var.app_name
    //keperr2 = var.something
  }
}

// Store Password in SSM Parameter Store
resource "aws_ssm_parameter" "rds_password" {
  name        = "/prod/postgresql"
  description = "Master Password for RDS"
  type        = "SecureString"
  value       = random_string.rds_password.result
  depends_on = [
    module.vpc-prod
  ]
}

// Get Password from SSM Parameter Store
data "aws_ssm_parameter" "my_rds_password" {
  name       = "/prod/postgresql"
  depends_on = [aws_ssm_parameter.rds_password]
}

module "rds" {
  depends_on = [
    module.vpc-prod
  ]
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 3.0"

  identifier = var.app_name

  engine            = "postgres"
  engine_version    = "11.6"
  instance_class    = "db.t2.micro"
  allocated_storage = 5

  create_db_parameter_group = false

  name     = "django"
  username = "django"
  password = data.aws_ssm_parameter.my_rds_password.value
  port     = "5432"

  iam_database_authentication_enabled = false

  vpc_security_group_ids = tolist([aws_security_group.rds-sg.id])

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = module.vpc-prod.database_subnets
}

module "ecs" {
  source                        = "./ecs"
  app_name                      = var.app_name
  app_port                      = var.api_port
  api_count                     = var.app_count
  frontend_count                = var.app_count
  db_user                       = module.rds.db_instance_username
  db_password                   = data.aws_ssm_parameter.my_rds_password.value
  db_url                        = module.rds.db_instance_address
  api_image                     = var.api_image
  frontend_image                = var.frontend_image
  fargate_cpu                   = 512
  fargate_memory                = 1024
  nlb_target_group_arn_api      = aws_lb_target_group.api_tg.arn
  alb_target_group_arn_frontend = aws_lb_target_group.alb_target_group_frontend.arn
  name                          = var.app_name
  cluster_tag_name              = var.app_name
  vpc_id                        = module.vpc-prod.vpc_id
  environment                   = var.environment
  aws_security_group_ecs_tasks_id = tolist([
    aws_security_group.ecs_tasks.id,
    aws_security_group.rds-sg.id
  ])
  private_subnet_ids = module.vpc-prod.private_subnets
  secret_key         = var.secret_key
  s3accesskey        = var.s3accesskey
  s3secretkey        = var.s3secretkey
  s3bucketname       = var.s3bucketname
  s3region           = var.s3region

  depends_on = [
    module.vpc-prod,
    module.rds,
    aws_security_group.ecs_tasks,
    aws_security_group.rds-sg,
    aws_lb_target_group.api_tg,
    aws_lb_target_group.alb_target_group_frontend,
    aws_vpc_endpoint.ecr_dkr,
    aws_vpc_endpoint.ecr_api,
    aws_vpc_endpoint.rds,
    aws_vpc_endpoint.cloudwatch,
    aws_vpc_endpoint.s3
  ]
}

module "dns" {
  source                 = "./dns_zone"
  domain_name            = "bryidomain.tk"
  alias_domain_name      = var.api_alias
  alb_domain_name        = aws_lb.alb.dns_name
  alb_zone_id            = aws_lb.alb.zone_id
  depends_on = [
    aws_lb.alb
  ]
}