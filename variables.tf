variable "app_name" {
  type        = string
  default     = "ecommerce"
  description = "Name of the container application"
}

variable "api_port" {
  description = "Application port"
  default     = 8000
  type        = number
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "The AWS region where resources have been deployed"
}

variable "availability_zones" {
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
  description = "List of availability zones for the selected region"
}

variable "environment" {
  description = "Applicaiton environment"
  default     = "dev"
  type        = string
}

variable "app_count" {
  default = 1
  type    = number
}

variable "api_image" {
  default = "249736386209.dkr.ecr.us-east-1.amazonaws.com/backend:latest"
}

variable "frontend_image" {
  default = "249736386209.dkr.ecr.us-east-1.amazonaws.com/frontend:latest"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "secret_key" {
  default = "f_)*$$6xz#a7k(6ir&u@+tq8h@_t_9%3nr%9g5z4vdp#*a4)a*o"
}

variable "s3accesskey" {
  default = "AKIATUJLS22QXMORTOXU"
}

variable "s3secretkey" {
  default = "zsnzSeQ+eihvX/iEIz/rDUV97UNyvC9rZ8gm8ZUh"
}

variable "s3bucketname" {
  default = "bryi-django-static"
}

variable "s3region" {
  default = "eu-north-1"
}

variable "api_alias" {
  default = "api.bryidomain.tk"
}