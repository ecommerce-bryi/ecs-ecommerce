data "aws_acm_certificate" "domain" {
  domain = "bryidomain.tk"
}

resource "aws_lb" "alb" {
  name               = "${var.app_name}-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb-sg.id]
  subnets            = module.vpc-prod.public_subnets

  depends_on = [
    module.vpc-prod
  ]
}

resource "aws_lb_listener" "alb_listener_ssl" {
  load_balancer_arn = aws_lb.alb.arn

  port     = 443
  protocol = "HTTPS"

  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = data.aws_acm_certificate.domain.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group_frontend.arn
  }

  depends_on = [
    aws_lb.alb
  ]
}

resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_lb_listener.alb_listener_ssl.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api_tg.arn
  }

  condition {
    path_pattern {
      values = [
        "/admin*", 
        "/api/*", 
        "health_check", 
      ]
    }
  }
}

resource "aws_lb_listener" "alb_listener_http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  depends_on = [
    aws_lb.alb
  ]
}

resource "aws_lb_target_group" "api_tg" {
  depends_on = [aws_lb.alb]
  name        = "${var.app_name}-tg-api"
  port        = var.api_port
  protocol    = "HTTP"
  vpc_id      = module.vpc-prod.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 2
    interval            = 30
    path                = "/health_check"
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

}

resource "aws_lb_target_group" "alb_target_group_frontend" {
  name        = "${var.app_name}-alb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = module.vpc-prod.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

  depends_on = [aws_lb.alb]

  lifecycle {
    create_before_destroy = true
  }
}